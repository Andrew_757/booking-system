<!-- Connect to Database -->
<?php include 'sql_connect.php';?>
<?php if ($_SESSION['username']==""){
     header("location: login.php");
}
?>
<?php
    if (isset($_POST['home_btn'])){
        
        $user = mysqli_real_escape_string($db, $_SESSION['username']);
        $classroom = mysqli_real_escape_string($db, $_POST['room']);
        $role = mysqli_real_escape_string($db, $_POST['role']);
        $projectname = mysqli_real_escape_string($db, $_POST['projectname']);
        $other = mysqli_real_escape_string($db, $_POST['other']);
        $specialequip = mysqli_real_escape_string($db, $_POST['specialequip']);
        $expequip = mysqli_real_escape_string($db, $_POST['expequip']);
        $datefrom = mysqli_real_escape_string($db, $_POST['datefrom']);
        $timefrom = mysqli_real_escape_string($db, $_POST['timefrom']);
        $dateto = mysqli_real_escape_string($db, $_POST['dateto']);
        $timeto = mysqli_real_escape_string($db, $_POST['timeto']);
        //prevent invalid time inputs
        $compareto = new DateTime($timeto);
        $comparefrom = new DateTime($timefrom);
        if($dateto==NULL)
        {
            $dateto = $datefrom;
        }
        if($timeto <= $timefrom && $datefrom == $dateto || $timefrom == null){
            $_SESSION['message']="Invalid time input";
        }
        else{
            echo "Today is " . date("Y,m,d") . "<br>";
            echo $dateto;
                //submit reservation to database
                $sqlres = "INSERT INTO reservations(user, classroom, role, projectname, other, specialequip, expequip, datefrom, timefrom, dateto, timeto) VALUES('$user', '$classroom', '$role', '$projectname', '$other', '$specialequip', '$expequip', '$datefrom', '$timefrom', '$dateto', '$timeto')";
                mysqli_query($db, $sqlres);
                $_SESSION['username'] = $user;
               // echo '<pre>' . print_r($_SESSION, TRUE) . '</pre>';
                mysqli_close($db);
                header("location: list.php");//redirect to list page   
        }
    }
    
?>
<!DOCTYPE html>
<meta charset="UTF-8">
<meta name="viewport" content="width=device-width,initial-scale=1.0">
<html>
    <head>
        <title>Booking Room</title>
        <!-- Latest compiled and minified CSS -->
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">

        <!-- Optional theme -->
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap-theme.min.css" integrity="sha384-rHyoN1iRsVXV4nD0JutlnGaslCJuC7uwjduW9SVrLvRYooPp2bWYgmgJQIXwl/Sp" crossorigin="anonymous">

        <!-- Latest compiled and minified JavaScript -->
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>

     
        <!-- JQuery -->
        <script type="text/javascript" src="jquery-3.2.1.js"></script>
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>

        <!-- Bootstrap Date-Picker Plugin -->
        <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.4.1/js/bootstrap-datepicker.min.js"></script>
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.4.1/css/bootstrap-datepicker3.css"/>
       <!-- Home CSS 
        <link rel="stylesheet" type="text/css" href="homestyle.css">-->
        <!-- Include Bootstrap-select CSS, JS -->
        <link rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.6.2/css/bootstrap-select.min.css" />
        <script src="//cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.6.2/js/bootstrap-select.min.js"></script>
        <!-- Timepicker -->
        <link rel="stylesheet" href="pickadate.js-3.5.6/lib/themes/default.css">
        <link rel="stylesheet" href="pickadate.js-3.5.6/lib/themes/default.date.css">
        <link rel="stylesheet" href="pickadate.js-3.5.6/lib/themes/default.time.css">
        <script type="text/javascript" src="pickadate.js-3.5.6/lib/picker.js"></script>
        <script type="text/javascript" src="pickadate.js-3.5.6/lib/picker.date.js"></script>    
        <script type="text/javascript" src="pickadate.js-3.5.6/lib/picker.time.js"></script>
        
        <!-- Booking CSS -->
        <link rel="stylesheet" type="text/css" href="booking.css">
        <!--script.js-->
        <script type="text/javascript" src="script.js"></script>
    </head>
    <body>
        
    <div class="col-sm-3"></div>
    <div class="bootstrap-iso">
 <div class="container-fluid">
  <div class="row">
   <div class="col-sm-6">
   <legend><span class="number">1</span>Welcome <?php echo $_SESSION['username']; ?>, make a reservation</legend>
       <?php
        if(isset($_SESSION['message'])){
        ?>
        <div class='error_msg'>
            <span class="closebtn" onclick="this.parentElement.style.display='none';">&times;</span> 
            <?php echo $_SESSION['message'];?>
        </div>
    <?php
        unset($_SESSION['message']);
        }
    ?>
    <!-- Form code begins -->
    <form method="post" action="reservation.php">    
      
     <!-- Choose classrom -->
   <label for="sel1">Classroom</label>
   <select class="form-control" id="sel1" name="room" value="room" title="Choose a room" onchange="roomfunction()">
       <?php
$sql = "SELECT * FROM classroom ORDER BY roomname ASC";
$result = mysqli_query($db, $sql);
 
 while($test = mysqli_fetch_array($result))
 {
 echo"<option>".$test['roomname']."</option>";
 }

 ?>
</select>
     <!-- End Choose classrom -->
     <!-- Select you role -->
        <div class="form-style-5">
        <fieldset id="group1">
<label for="type">Select your role</label>
    
    <div class="radio">
  <label>
    <input type="radio" name="role" id="radioteaching" value="Teaching" onclick="check()" required>
    Teaching
  </label>
</div>
<div class="radio">
  <label>
    <input type="radio" name="role" id="radioresearch" value="Research" onclick="check()" >
    Research
  </label>
</div>
    <div class="radio">
  <label>
    <input type="radio" name="role" id="radiomaintenance" value="Maintenance" onclick="check()">
    Maintenance
  </label>
</div>
<div class="radio">
  <label>
    <input type="radio" name="role" id="radiodemonstration" value="Demonstration" onclick="check()">
    Demonstration
  </label>
</div>
    <div class="radio">
  <label>
    <input type="radio" name="role" id="radioother" value="Other" onclick="check()">
    Other
  </label>
</div> 
</fieldset>
<fieldset>
    <div id="projectname">
    <input type="text" name="projectname" placeholder="Project Name">
    </div>
    </fieldset>
<fieldset>
    <div id="other">
    <input type="text" name="other" placeholder="Please, specify">
    </div>
    </fieldset>
<fieldset id="group2">
<label for="type">Will you be using any special equipment?</label>

 <div class="radio">
  <label>
    <input type="radio" name="specialequip" id="radioyes" value="Yes" onclick="check2()" required>
    Yes
  </label>
</div>
<div class="radio">
  <label>
    <input type="radio" name="specialequip" id="radiono" value="No" onclick="check2()">
    No
  </label>
</div>
<fieldset>
    <div id="expequip">
    <select class="form-control" name="expequip" value="expequip" title="Specify which one">
       <?php
$sql3 = "SELECT roomname FROM classroom ORDER BY roomname ASC";
$result3 = mysqli_query($db, $sql3);
while($test3 = mysqli_fetch_array($result3))
 {
 echo"<optgroup label=".$test3['roomname'].">";
 $roomtest = $test3['roomname'];
$sql2 = "SELECT equipname FROM equipment WHERE room='$roomtest' ORDER BY equipname ASC";
$result2 = mysqli_query($db, $sql2);
 while($test2 = mysqli_fetch_array($result2))
 {
 echo"<option>".$test2['equipname']."</option>";
 }
 echo"</optgroup>";
 }
 ?>
        </select>
    </div>
    </fieldset>    
</fieldset>
        </div>
         <!--Date picker FROM-->
        <div class="from">
        <label>From</label>
       <div class="date">
       <input class="datepicker" name="datefrom" type="text" id="prueba1" placeholder="Choose a date" required>
        <!-- Date configuration -->
        <script>
        $('.datepicker').pickadate({
          // Escape any “rule” characters with an exclamation mark (!).
          firstDay: 1,
          max: +30,
          min: true,
          format: 'dddd, dd mmm, yyyy',
          formatSubmit: 'yyyy,mm,dd',
          hiddenPrefix: 'prefix__',
          hiddenSuffix: '__suffix',
        
              
            onStart: function ()
          {
              var date = new Date();
             this.set('select', [date.getFullYear(), date.getMonth(), date.getDate()]);
          },
          hiddenName: true
        })
        </script>
       <script>$('.datepicker').pickadate();</script>
       </div>
        <!-- Time picker FROM -->
          <div class="time">
       <input class="timepicker" type="text" name="timefrom" placeholder="Choose a time">
               <?php
                $sql = "SELECT classroom, datefrom, timefrom, dateto, timeto FROM reservations WHERE dateto >= CURDATE() ORDER BY datefrom ASC";
                $result = mysqli_query($db, $sql); 
                $counter = 0;
                $classroomArray = array();
                $datefromArray = array();
                $timefromArray = array();
                $datetoArray = array();
                $timetoArray = array();
                while($test = mysqli_fetch_array($result))
                {   
                    $classroomArray[$counter]=$test['classroom'];
                    $datefromArray[$counter]=$test['datefrom'];
                    $timefromArray[$counter]=$test['timefrom'];
                    $datetoArray[$counter]=$test['dateto'];
                    $timetoArray[$counter]=$test['timeto'];
                    $counter = $counter+1;
                }
              ?>
              
       <script>$('.timepicker').pickatime({
          format: 'HH:i',
          interval: 60,      
          onOpen: function ()
          {    
              // creattion of a temporal variable to transform the date to the correct format, from yyyy/m/dd to yyyu/mm/dd 
              var temp = new Date(document.getElementById("prueba1").value);
              var checkMonth = temp.getMonth()+1;
              var checkDay = temp.getDate();
              // change if incorrect
              if(checkMonth<=9){checkMonth ="0"+checkMonth;}
              if(checkDay<=9){checkDay ="0"+checkDay;}
              var correctDate =temp.getFullYear()+"-"+checkMonth+"-"+checkDay;
              var roomSelected = document.getElementById('sel1').value;
               console.log(correctDate);
               console.log("compare to:");
              this.set('enable', true);
                            
              var i;
              for (i=1; i< datefromArrayjs.length;i++){
                  console.log(datefromArrayjs[i]);
                  // if the selected date and class is equal to any of the reservations disable the hours of that reservation
                  if(correctDate.localeCompare(datefromArrayjs[i])== 0 && roomSelected.localeCompare(classroomArrayjs[i])== 0 ){
                     console.log("are equal");
                      if(datefromArrayjs[i]==datetoArrayjs[i]){
                           console.log(timefromArrayjs[i]);
                           var tf = timefromArrayjs[i].substring(0,2);
                           var tt = timetoArrayjs[i].substring(0,2);
                           this.set('disable', [{from: [tf,0], to: [--tt,0]}]);
                      } 
                      
                  }
                  
              }
              
            
              }, 
               
           });
            //save the php variables in javascript for access information to disable already booked times
                    //javascript array with datefrom data
                     var datefromArrayjs = [ "0" <?php
                    $input =0;
                    while($input< $counter){
                         echo ",";

                        echo '"';
                        echo $datefromArray[$input];
                        echo '"';
                        $input = $input + 1;
                    }?>];
                    //javascript array with timefrom data
                     var timefromArrayjs = [ "1" <?php
                    $input2 =0;
                    while($input2< $counter){
                         echo ",";

                        echo '"';
                        echo $timefromArray[$input2];
                        echo '"';
                        $input2 = $input2 + 1;
                    }?>]; 
                    //javascript array with dateto data
                    var datetoArrayjs = [ "2" <?php
                    $input3 =0;
                    while($input3< $counter){
                         echo ",";

                        echo '"';
                        echo $datetoArray[$input3];
                        echo '"';
                        $input3 = $input3 + 1;
                    }?>];
                    //javascript array with timeto data
                    var timetoArrayjs = [ "3" <?php
                    $input4 =0;
                    while($input4< $counter){
                         echo ",";

                        echo '"';
                        echo $timetoArray[$input4];
                        echo '"';
                        $input4 = $input4 + 1;
                    }?>]; 
                    //javascript array with classroom data
                    var classroomArrayjs = [ "4" <?php
                    $input5 =0;
                    while($input5< $counter){
                         echo ",";

                        echo '"';
                        echo $classroomArray[$input5];
                        echo '"';
                        $input5 = $input5 + 1;
                    }?>]; 
           
           
           
           
        </script>
        </div>
            </div>
      <div class="checkbox">
      <label><input type="checkbox" id="checkmoredays" onclick="moredaysfunc()" value="">I want to reserve a room for more than a day</label>
    </div>
        <div class="to">
        <Label>To</Label>
        <div id="moredays" class="date">
       <input class="datepicker" type="text" name="dateto" id="prueba2" placeholder="Choose a date">
        <!-- Date picker TO -->
        <script>
        $('.datepicker').pickadate({
          // Escape any “rule” characters with an exclamation mark (!).
          firstDay: 1,
          max: +60,
          min: new Date(document.getElementById("prueba1").value),
          format: 'dddd, dd mmm, yyyy',
          formatSubmit: 'yyyy/mm/dd',
          hiddenPrefix: 'prefix__',
          hiddenSuffix: '__suffix',
          hiddenName: true,
           onOpen: function ()
          {
                
                this.set('min', new Date(document.getElementById("prueba1").value));
                //this.set('max', new Date(document.getElementById("prueba1").value) + 10);
          }
        })
        </script>
       <script>$('.datepicker').pickadate();</script>
        </div>
            
        <!-- Time picker TO -->
       <div class="time">
       <input class="timepicker" type="text" name="timeto" placeholder="Choose a time" required>
       <script>$('.timepicker').pickatime({
               format: 'HH:i',
               interval: 60,
                   onOpen: function ()
          {    
              // creattion of a temporal variable to transform the date to the correct format, from yyyy/m/dd to yyyu/mm/dd 
              var temp = new Date(document.getElementById("prueba1").value);
              var checkMonth = temp.getMonth()+1;
              var checkDay = temp.getDate();
              // change if incorrect
              if(checkMonth<=9){checkMonth ="0"+checkMonth;}
              if(checkDay<=9){checkDay ="0"+checkDay;}
              var correctDate =temp.getFullYear()+"-"+checkMonth+"-"+checkDay;
              var roomSelected = document.getElementById('sel1').value;
               console.log(correctDate);
               console.log("compare to:");
              this.set('enable', true);
                            
              var i;
              for (i=1; i< datefromArrayjs.length;i++){
                  console.log(datefromArrayjs[i]);
                  // if the selected date and class is equal to any of the reservations disable the hours of that reservation
                  if(correctDate.localeCompare(datefromArrayjs[i])== 0 && roomSelected.localeCompare(classroomArrayjs[i])== 0 ){
                     console.log("are equal");
                      if(datefromArrayjs[i]==datetoArrayjs[i]){
                           console.log(timefromArrayjs[i]);
                           var tf = timefromArrayjs[i].substring(0,2);
                           var tt = timetoArrayjs[i].substring(0,2);
                           this.set('disable', [{from: [++tf,0], to: [tt,0]}]);
                      } 
                      
                  }
                  
              }
              
            
              }, 
           });</script>
       </div>
            </div>
        <!-- End Time picker -->
         
        <div class="form-group"> 
            <!-- Submit button -->
        <button class="btn btn-default" name="home_btn" type="submit" value="Home">Submit</button>
      </div>
        <!-- Form code ends -->
        </form>
      <div class="end">
        <a href="logout.php">Logout</a>
        <a> | </a>
        <a href="list.php">Display reservations</a>  
     </div>
    </div>
  </div>    
 </div>
</div>
        <div class="col-sm-3"></div>
    </body>
</html>		