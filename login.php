<!-- Connect to database -->
<?php include 'sql_connect.php';?>  
  <?php
    if (isset($_POST['login_btn'])){
     
        $username = mysqli_real_escape_string($db, $_POST['username']);
        $password = mysqli_real_escape_string($db, $_POST['password']);
        
        $password = hash("sha512", $password); // remember we hashed password before storing 
        
        $sql = "SELECT * FROM users WHERE username='$username' AND password= '$password'";
        $result = mysqli_query($db, $sql);
        $row=mysqli_fetch_array($result,MYSQLI_BOTH);
        //if user and password is correct
        if (mysqli_num_rows($result) == 1){
            $_SESSION['message'] = "You are now logged in";
            // the first argument is the username
            $_SESSION['username'] = $row[1];
            header("location: list.php"); // redirect to list page
           
        }else{
            $_SESSION['message'] = "Username/password combination incorrect";
        }
    }
?>
<!DOCTYPE html>
<meta charset="UTF-8">
<meta name="viewport" content="width=device-width,initial-scale=1.0">
<html>
<head>
    <title>Login</title>
    <!-- Bootstrap CDN -->
    <!-- Latest compiled and minified CSS -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">

    <!-- Optional theme -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap-theme.min.css" integrity="sha384-rHyoN1iRsVXV4nD0JutlnGaslCJuC7uwjduW9SVrLvRYooPp2bWYgmgJQIXwl/Sp" crossorigin="anonymous">

    <!-- Latest compiled and minified JavaScript -->
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>
    <link rel="stylesheet" type="text/css" href="homestyle.css">
</head>
<body> 
<!-- Form Starts -->
<div class="form-style-5">
<form method="post" action = login.php>
<fieldset>
<legend><span class="number">1</span> Login</legend>
<?php
    //Display error message
    if(isset($_SESSION['message'])){
        ?>
        <div class='error_msg'>
            <span class="closebtn" onclick="this.parentElement.style.display='none';">&times;</span> 
            <?php echo $_SESSION['message'];?>
        </div>
    <?php
        unset($_SESSION['message']);
    }
?>
<input type="text" name="username" class="textInput" placeholder="Your Name" required>
<input type="password" name="password" class="textInput" placeholder="Your Password" required>
</fieldset>
<input type="submit" class="btn btn-default" name="login_btn" value="Login" />
    <!-- Link to register.php -->
<a href="register.php" class="btn btn-default" type="submit">Sign up</a>
</form>
</div>
</body>
</html>			