
-- phpMyAdmin SQL Dump
-- version 3.5.2.2
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: May 28, 2017 at 02:09 PM
-- Server version: 10.0.28-MariaDB-wsrep
-- PHP Version: 5.2.17

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `u717350551_db`
--

-- --------------------------------------------------------

--
-- Table structure for table `classroom`
--

CREATE TABLE IF NOT EXISTS `classroom` (
  `roomname` varchar(20) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`roomname`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `classroom`
--

INSERT INTO `classroom` (`roomname`) VALUES
('A008'),
('A009'),
('A010'),
('A011'),
('A012'),
('A045');

-- --------------------------------------------------------

--
-- Table structure for table `equipment`
--

CREATE TABLE IF NOT EXISTS `equipment` (
  `id_equip` int(11) NOT NULL AUTO_INCREMENT,
  `equipname` varchar(300) COLLATE utf8_unicode_ci NOT NULL,
  `room` varchar(20) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id_equip`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=88 ;

--
-- Dumping data for table `equipment`
--

INSERT INTO `equipment` (`id_equip`, `equipname`, `room`) VALUES
(1, 'Dell CPU RN 03440', 'A011'),
(2, 'Dell CPU RN 06113', 'A011'),
(3, 'EIZO Monitor', 'A011'),
(4, 'Nec Spectra view LCD monitor RN 04116', 'A011'),
(5, 'Dell laptop RN 05832', 'A011'),
(6, 'DELL laptop docking station RN05834', 'A011'),
(7, 'Oculus hmc anamaloskop', 'A011'),
(37, 'Radiant Photon bread flash light', 'A008'),
(34, 'Dell CPU RN 04618', 'A008'),
(15, 'OCE colorwave 600PP', 'A012'),
(16, 'HP design jet 5000ps', 'A012'),
(17, 'Xerox Phaser 6250', 'A012'),
(18, 'GretagMacbeth Judge II Booth', 'A012'),
(19, 'Just Normlicht colorControl Booth', 'A012'),
(20, 'GretagMacbeth spectroscan', 'A012'),
(21, 'Epson Expression XL scanner', 'A012'),
(22, 'BARBIERI Electronic', 'A012'),
(23, 'OCE colorwave 600PP table', 'A012'),
(42, 'GretagMacbeth Spectralight III Booth', 'A010'),
(41, 'HP Photosmart pro B9180', 'A010'),
(40, 'HP Designjet 10ps RN 03217', 'A010'),
(39, 'HP Designjet Z3200ps Photo', 'A010'),
(33, 'Dell Monitor RN 02811', 'A008'),
(38, 'Highlight photon bread flash light', 'A008'),
(43, 'Just Normlicht colorCommunicator 2 Booth', 'A010'),
(35, 'Elinchrom BRX 500 Flash light RN 05099', 'A008'),
(36, 'Elinchrom BRX 500 Flash light RN 0 6619', 'A008'),
(44, 'GretagMacbeth io Electrostatic Mat', 'A010'),
(45, 'Macbeth PLT1620', 'A010'),
(46, 'Dell monitor RN 06116', 'A010'),
(47, 'CPU Dell Precision T7600 RN 05971', 'A010'),
(48, 'iMac RN 06069', 'A010'),
(49, 'DELL Latitude laptop 5838', 'A010'),
(50, 'CPU iMac RN 02188', 'A010'),
(51, 'DELL Monitor RN 06114', 'A010'),
(52, 'Microscope', 'A010'),
(53, 'LaCIE Monitor', 'A010'),
(54, 'TMc300 Monochromator - Bentham Instruments Ltd. SN 7705 IL1 - ENC, SNo 7700, Operate at - 8.500A – illuminator Integrating sphere SN 7711', 'A045'),
(55, 'Verivide booth', 'A009'),
(56, 'White writing board', 'A009'),
(57, 'Projector screen', 'A009'),
(58, 'Sony 3LCD projector RN 04468', 'A009'),
(59, 'Dell XPS CPU RN 04475', 'A009'),
(60, 'Dell Monitor RN 04002', 'A009'),
(61, 'Dell Monitor RN 05119', 'A009'),
(62, 'Benq Monitor RN 07439', 'A009'),
(63, 'Geha Top Vision 2400', 'A009'),
(64, 'Dell CPU RN 05915', 'A009'),
(65, 'Conference equipment', 'A009'),
(66, 'Mac PC RN 02182', 'A009'),
(68, 'Bentham 605 Power supply - 75951', 'A045'),
(69, 'Bentham 417 detection electronics base unit SN 7703', 'A045'),
(70, 'Dell PC Optiplex GX620 RN 03921', 'A045'),
(71, 'M300BA Monochromator Bentham, SN4001  IL1 - ENC SN 3906/5  Detector Head, DH2, 3722/3', 'A045'),
(72, 'Bentham 417, 217T/PMCM SN 4010/1 (Pro)', 'A045'),
(73, 'Bentham 605, Power supply', 'A045'),
(74, 'Mac PC RN 02190', 'A045'),
(75, 'Scanner Agfa Arcus 1200, RN 02449', 'A045'),
(76, 'Negatives Frame', 'A045'),
(77, 'GONIO - 2M, SN 4000  black box with foldable mirror', 'A045'),
(78, 'Verivide Cac 60-5, Light Booth', 'A045'),
(79, 'Sony 3LCD Projector, RN 04470', 'A045'),
(80, 'Nikon Digital Camera D610, RN HIG 09718', 'A045'),
(81, 'Nikon lense RN HIG 09725', 'A045'),
(82, '2X Nikon charger  2X Camera bag - gestobags.com', 'A045'),
(83, 'Nikon Digital Camera D3200, RN 09717', 'A045'),
(84, 'Lense Sigma with sun shield', 'A045'),
(85, 'Camera tripod TH-650', 'A045'),
(86, 'Systemax PC', 'A045'),
(87, 'White board', 'A045');

-- --------------------------------------------------------

--
-- Table structure for table `reservations`
--

CREATE TABLE IF NOT EXISTS `reservations` (
  `resID` int(100) NOT NULL AUTO_INCREMENT,
  `user` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `classroom` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `role` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `projectname` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `other` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `specialequip` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `expequip` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `datefrom` date NOT NULL,
  `timefrom` time NOT NULL,
  `dateto` date NOT NULL,
  `timeto` time NOT NULL,
  PRIMARY KEY (`resID`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=201 ;

--
-- Dumping data for table `reservations`
--

INSERT INTO `reservations` (`resID`, `user`, `classroom`, `role`, `projectname`, `other`, `specialequip`, `expequip`, `datefrom`, `timefrom`, `dateto`, `timeto`) VALUES
(199, 'David', 'A011', 'Teaching', '', '', 'No', 'Dell CPU RN 04618', '2017-05-23', '10:00:00', '2017-05-23', '14:00:00'),
(200, 'David', 'A011', 'Research', '', '', 'Yes', 'Dell CPU RN 04618', '2017-05-23', '11:00:00', '2017-05-20', '13:00:00'),
(185, 'David', 'A010', 'Research', 'Booking System', '', 'Yes', 'CPU Dell Precision T7600 RN 05971', '2017-05-11', '10:00:00', '2017-05-11', '13:00:00'),
(186, 'Andres', 'A009', 'Demonstration', '', '', 'Yes', 'Benq Monitor RN 07439', '2017-05-18', '09:00:00', '2017-05-18', '13:00:00'),
(187, 'Fayan', 'A012', 'Other', '', 'Testing', 'No', 'Dell CPU RN 04618', '2017-05-12', '16:00:00', '2017-05-12', '18:00:00'),
(188, 'Jorge', 'A011', 'Maintenance', '', '', 'No', 'Dell CPU RN 04618', '2017-05-13', '09:00:00', '2017-05-15', '09:00:00'),
(189, 'Stefan', 'A010', 'Teaching', '', '', 'Yes', 'GretagMacbeth Spectralight III Booth', '2017-05-12', '08:00:00', '2017-05-12', '12:00:00'),
(198, 'Jorge', 'A008', 'Research', '', '', 'No', 'Dell CPU RN 04618', '2017-05-17', '12:00:00', '2017-05-17', '15:00:00'),
(192, 'David', 'A045', 'Demonstration', '', '', 'No', 'Dell CPU RN 04618', '2017-05-24', '14:00:00', '2017-05-24', '16:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE IF NOT EXISTS `users` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `username` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `NFC` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `password` varchar(250) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `username` (`username`),
  UNIQUE KEY `email` (`email`),
  UNIQUE KEY `id` (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=73 ;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `username`, `email`, `NFC`, `password`) VALUES
(68, 'Andres', 'a@mail.com', '', '1f40fc92da241694750979ee6cf582f2d5d7d28e18335de05abc54d0560e0f5302860c652bf08d560252aa5e74210546f369fbbbce8c12cfc7957b2652fe9a75'),
(69, 'Fayan', 'f@mail.com', '', '711c22448e721e5491d8245b49425aa861f1fc4a15287f0735e203799b65cffec50b5abd0fddd91cd643aeb3b530d48f05e258e7e230a94ed5025c1387bb4e1b'),
(70, 'Jorge', 'j@mail.com', '', 'fcd8780493d9d11d29031b928a9da358a6f48627fff0cb7e80fb8107de86e0c365dc9cf8fe2fc05a6ce6d75803b78ac894d82a396042312a995ef63b5dd4dd11'),
(71, 'Stefan', 's@mail.com', '', '2c1ee68372215b1ce064426b5cdbd4ef2581ace0dd3b21fa2be27f364827242e83f68b68be03f5b3e24be5d1b4315f98a0a96d19713fb3a19dc455fb6adc3431'),
(67, 'David', 'd@mail.com', '0xfbe6f0110439f4c3a1aed6c76bc30ba6af57c13e79315b9cd6ba87836e9654b0', '48fb10b15f3d44a09dc82d02b06581e0c0c69478c9fd2cf8f9093659019a1687baecdbb38c9e72b12169dc4148690f87467f9154f5931c5df665c6496cbfd5f5');

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
