<!-- Connect to database -->
<?php include 'sql_connect.php';?>
<?php
    if (isset($_POST['register_btn'])){

        $username = mysqli_real_escape_string($db, $_POST['username']);
        $email = mysqli_real_escape_string($db, $_POST['email']);
        $password = mysqli_real_escape_string($db, $_POST['password']);
        $password2 = mysqli_real_escape_string($db, $_POST['password2']);
        
        //Check if user already exists
        $query = "SELECT * FROM users WHERE username='$username'";
        $result = mysqli_query($db, $query);
        $count = mysqli_num_rows($result);
        //Check if email already exists
        $query2 = "SELECT * FROM users WHERE email='$email'";
        $result2 = mysqli_query($db, $query2);
        $count2 = mysqli_num_rows($result2);
        
        if($password == $password2 && $count == 0 && $count2 == 0){
            //create user
            $password = hash("sha512", $password); //hash pass before storing for security
            $sql = "INSERT INTO users(username, email, password) VALUES('$username', '$email', '$password')";
            mysqli_query($db, $sql);
            $query3 = "SELECT * FROM users WHERE username='$username' AND password = '$password'";
            $result3 = mysqli_query($db, $query3);
            $row = mysqli_fetch_array($result3,MYSQLI_BOTH);
            $_SESSION['message'] = "You are now logged in";
            //the first argument is the username
            $_SESSION['username'] = $row[1];
            echo '<pre>' . print_r($_SESSION, TRUE) . '</pre>';
            header("location: list.php");//redirect to list page
        }else if($count>0) {
            $_SESSION['message'] = "Sorry! This Username already exists!";
            
        }else if($count2>0){
            $_SESSION['message'] = "Sorry! This e-mail already exists!";
        }
        else{
            //failed
            $_SESSION['message'] = "The two passwords do not match";
            
        }

    }
?>
<!DOCTYPE html>
<meta charset="UTF-8">
<meta name="viewport" content="width=device-width,initial-scale=1.0">
<html>
<head>
    <title>Register</title> 
     <title>Register</title>
    <!-- Bootstrap CDN -->
    <!-- Latest compiled and minified CSS -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">

    <!-- Optional theme -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap-theme.min.css" integrity="sha384-rHyoN1iRsVXV4nD0JutlnGaslCJuC7uwjduW9SVrLvRYooPp2bWYgmgJQIXwl/Sp" crossorigin="anonymous">

    <!-- Latest compiled and minified JavaScript -->
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>
    <!-- homestyle CSS file -->
    <link rel="stylesheet" type="text/css" href="homestyle.css">
</head>
<body>
<!-- Form starts -->
<div class="form-style-5">
<form method="post" action = register.php>
<fieldset>
<legend><span class="number">1</span> Register</legend>
<?php
    //Display error message
    if(isset($_SESSION['message'])){
        ?>
        <div class='error_msg'>
            <span class="closebtn" onclick="this.parentElement.style.display='none';">&times;</span> 
            <?php echo $_SESSION['message'];?>
        </div>
    <?php
        unset($_SESSION['message']);
    }
?>
<input type="text" name="username" class="textInput" placeholder="Your Name" required>
<input type="email" name="email" class="textInput" placeholder="Your Email" required>  
<input type="password" name="password" class="textInput" placeholder="Your Password" required>
<input type="password" name="password2" class="textInput" placeholder="Confirm Your Password" required>
</fieldset>
<input type="submit" class="btn btn-default" name="register_btn" value="Register" />
<!-- Link to Login.php -->
<a href="login.php" class="btn btn-default" type="submit">Login</a>
</form>
</div>

</body>
</html>