package the9thfloor.nfc;

import android.app.PendingIntent;
import android.content.Intent;
import android.content.IntentFilter;
import android.net.Uri;
import android.nfc.NfcAdapter;
import android.nfc.Tag;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.Toast;
import java.nio.charset.StandardCharsets;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

public class MainActivity extends AppCompatActivity {

    NfcAdapter nfcAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        // inicialization of variables
        nfcAdapter = NfcAdapter.getDefaultAdapter(this);

        // check is the device have NFC and that is activated
        if (nfcAdapter!=null && nfcAdapter.isEnabled()){
            Toast.makeText(this, "NFC avalible", Toast.LENGTH_LONG).show();
        } else{
            Toast.makeText(this, "NFC not avalible", Toast.LENGTH_LONG).show();
           // finish();
        }

    }

    @Override
    protected void onNewIntent(Intent intent) {
            //When an intent is received it looks is it a NFC

            super.onNewIntent(intent);
            if (intent.hasExtra(NfcAdapter.EXTRA_TAG)) {
                Toast.makeText(this, "NfcIntent!", Toast.LENGTH_SHORT).show();

                if (intent.hasExtra(NfcAdapter.EXTRA_TAG)) {
                    //Create a tag with the data received with the intent
                    Tag myTag = intent.getParcelableExtra(NfcAdapter.EXTRA_TAG);

                    //send the Tag Id using the function send()
                    send( bytesToHexString(myTag.getId()));

                }
            }
    }


    @Override
    protected void onResume() {
        Intent intent = new Intent(this, MainActivity.class);
        intent.addFlags(Intent.FLAG_RECEIVER_REPLACE_PENDING);

        PendingIntent pendingIntent = PendingIntent.getActivity(this,0,intent,0);

        IntentFilter[] intentFilter = new IntentFilter[2];
        intentFilter[0]= new IntentFilter();
        intentFilter[0].addAction(NfcAdapter.ACTION_TECH_DISCOVERED);
        intentFilter[0].addCategory(Intent.CATEGORY_DEFAULT);
        intentFilter[1]= new IntentFilter();
        intentFilter[1].addAction(NfcAdapter.ACTION_TAG_DISCOVERED);
        intentFilter[1].addCategory(Intent.CATEGORY_DEFAULT);

        if(nfcAdapter!=null){

            nfcAdapter.enableForegroundDispatch(this, pendingIntent,intentFilter,null);
        }
        super.onResume();

    }
/*
    @Override
    protected void onPause()  {
        nfcAdapter.disableForegroundDispatch(this);
        super.onPause();
    }*/

    /* transform bytes array into hexadecimal string
    code from: http://stackoverflow.com/questions/6060312/how-do-you-read-the-unique-id-of-an-nfc-tag-on-android
    */
    private String bytesToHexString(byte[] src) {
        StringBuilder stringBuilder = new StringBuilder("0x");
        if (src == null || src.length <= 0) {
            return null;
        }

            char[] buffer = new char[2];
            for (int i = 0; i < src.length; i++) {
                buffer[0] = Character.forDigit((src[i] >>> 4) & 0x0F, 16);
                buffer[1] = Character.forDigit(src[i] & 0x0F, 16);
                System.out.println(buffer);
                stringBuilder.append(buffer);
            }

        return stringBuilder.toString();

    }
    // sends the id to the webpage make the login
    public void send(String id)
    {
        String params;
        MessageDigest digest = null;
        try {
            //encrypt the ID using SHA-256
            digest = MessageDigest.getInstance("SHA-256");
            byte[] hash = digest.digest(id.getBytes(StandardCharsets.UTF_8));
            params = "?x="+bytesToHexString(hash);
            String url = "http://queue.16mb.com/nfc.php" + params;
            // open browser with the NFC ID (encrypted) as a variable
            Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(url));
            startActivity(browserIntent);
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        }
    }
}
