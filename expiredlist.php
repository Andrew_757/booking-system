<!-- Connect to database -->
<?php include 'sql_connect.php';?>
 <!doctype html>
<html>
<head>
<meta http-equiv="Content-type" content="text/html; charset=utf-8">
<meta name="viewport" content="width=device-width,initial-scale=1">
<title>List of reservations</title>
    
<!-- Bootstrap CDN -->
<link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css">

<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.0/jquery.min.js"></script>

<script src="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"></script>

     <!-- Latest compiled and minified CSS -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">

    <!-- Optional theme -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap-theme.min.css" integrity="sha384-rHyoN1iRsVXV4nD0JutlnGaslCJuC7uwjduW9SVrLvRYooPp2bWYgmgJQIXwl/Sp" crossorigin="anonymous">

    <!-- Latest compiled and minified JavaScript -->
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>
    
    <!-- JQuery -->
     <script type="text/javascript" src="jquery-3.2.1.js"></script>
     <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
    
    <!-- Datatables CDN -->
    <link rel="stylesheet" type="text/css" href="http://cdn.datatables.net/1.10.15/css/jquery.dataTables.css">
    <link rel="stylesheet" href="http://cdn.datatables.net/plug-ins/a5734b29083/integration/bootstrap/3/dataTables.bootstrap.css"/>
    <link rel="stylesheet" href="http://cdn.datatables.net/responsive/1.0.2/css/dataTables.responsive.css"/>
    
       <!-- Booking css -->
    <link rel="stylesheet" type="text/css" href="booking.css">
    
    

</head>
        <div class="col-sm-0 col-md-0 col-lg-1">
    </div>
<body>
        <div class="col-sm-12 col-md-12 col-lg-10">
            <div class="form-style-5"> 
                <div class="container">
                    <?php if ($_SESSION['username']==""){
                        $us = "Guest";
                    }else{
                        $us = $_SESSION['username'];
                    }
                    ?>
                    <br><legend><span class="number">1</span>Welcome <?php echo $us; ?></legend>
                    
<table id="example" class="table table-striped table-hover display dt-responsive nowrap" cellspacing="0" width="100%">
 <thead>
 <tr>
 <th>User</th>
 <th class="all">Classroom</th>
 <th>Role</th>
 <th class="all">Special equipment</th>
 <th>From Date</th>
 <th>From Time</th>
 <th>To Date</th>
 <th>To Time</th>
 </tr>  
 </thead>
 <tbody>
 <tr>
 <?php
//Get the reservations that are not expired
$sql = "SELECT * FROM reservations WHERE dateto < CURDATE() ORDER BY datefrom ASC";
$result = mysqli_query($db, $sql);
 
 while($test = mysqli_fetch_array($result))
 {
 echo"<td>".$test['user']."</td>";
 echo"<td>".$test['classroom']."</td>";
//In case Research is selected, it will also display the name of the project
 if($test['projectname']!="" && $test['role']=="Research")
 {
 echo"<td>Research: ".$test['projectname']."</td>"; 
 }
//In case Other is selected, it will also display the specification
 else if($test['other']!="" && $test['role']=="Other")
 {
 echo"<td>Other: ".$test['other']."</td>"; 
 }
 else
 {
  echo"<td>".$test['role']."</td>";    
 }
 if($test['specialequip']=="Yes")
 {
 echo"<td style='max-width: 150px; overflow: hidden; text-overflow: ellipsis;'>".$test['expequip']."</td>"; 
 }
 else
 {
 echo"<td>".$test['specialequip']."</td>"; 
 }     
 echo"<td>".$test['datefrom']."</td>"; 
 echo"<td>".$test['timefrom']."</td>";
 echo"<td>".$test['dateto']."</td>"; 
 echo"<td>".$test['timeto']."</td>"; 
 ?>
 </tr>
 <?php
 }
 mysqli_close($db);
 ?>
     </tbody>
</table>
</div>
<br><a style="max-width:300px; padding:10px;" href="reservation.php" class="btn btn-default" type="submit">Make a Reservation</a>
<div class="end">
<a href="logout.php">Logout</a>
<a> | </a>
<a href="list.php">Show current reservations</a>  
</div>
         

<br>
</div>
</div>
    <div class="col-sm-0 col-md-0 col-lg-1">
    </div>
    <!-- Datatables CDN -->
        <script type="text/javascript" language="javascript" src="//cdn.datatables.net/1.10.3/js/jquery.dataTables.min.js"></script>
        <script type="text/javascript" language="javascript" src="//cdn.datatables.net/responsive/1.0.2/js/dataTables.responsive.js"></script>
        <script type="text/javascript" language="javascript" src="//cdn.datatables.net/plug-ins/a5734b29083/integration/bootstrap/3/dataTables.bootstrap.js"></script>
        <!-- Datatables configuration -->
        <script>
            $(document).ready(function() {
            $('#example').DataTable({
                responsive: true,
                "paging":   false,
                "aaSorting": [[4, 'asc']],
            } ); 
            } );

            </script>
</body>
</html>