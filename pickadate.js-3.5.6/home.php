<?php
    session_start();
    //connect to database
    $db = mysqli_connect('mysql.hostinger.es', 'u717350551_user', '123456', 'u717350551_db');
    if (isset($_POST['home_btn'])){
        
        $user = mysqli_real_escape_string($db, $_SESSION['username']);
        $classroom = mysqli_real_escape_string($db, $_POST['room']);
        $role = mysqli_real_escape_string($db, $_POST['role']);
        $projectname = mysqli_real_escape_string($db, $_POST['projectname']);
        $other = mysqli_real_escape_string($db, $_POST['other']);
        $specialequip = mysqli_real_escape_string($db, $_POST['specialequip']);
        $expequip = mysqli_real_escape_string($db, $_POST['expequip']);
        $datefrom = mysqli_real_escape_string($db, $_POST['datefrom']);
        $timefrom = mysqli_real_escape_string($db, $_POST['timefrom']);
        $dateto = mysqli_real_escape_string($db, $_POST['dateto']);
        $timeto = mysqli_real_escape_string($db, $_POST['timeto']);
        
        if($dateto==NULL)
        {
            $dateto = $datefrom;
        }
        echo "Today is " . date("Y,m,d") . "<br>";
        echo $dateto;
            //submit reservation to database
            $sqlres = "INSERT INTO reservations(user, classroom, role, projectname, other, specialequip, expequip, datefrom, timefrom, dateto, timeto) VALUES('$user', '$classroom', '$role', '$projectname', '$other', '$specialequip', '$expequip', '$datefrom', '$timefrom', '$dateto', '$timeto')";
            mysqli_query($db, $sqlres);
            $_SESSION['message'] = "Reservation succesfully made";
            $_SESSION['username'] = $user;
           // echo '<pre>' . print_r($_SESSION, TRUE) . '</pre>';
            header("location: list.php");//redirect to list page   
        }

    
?>
<!DOCTYPE html>
<meta charset="UTF-8">
<meta name="viewport" content="width=device-width,initial-scale=1.0">
<html>
    <head>
        <title>Booking Room</title>
        <!-- Latest compiled and minified CSS -->
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">

        <!-- Optional theme -->
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap-theme.min.css" integrity="sha384-rHyoN1iRsVXV4nD0JutlnGaslCJuC7uwjduW9SVrLvRYooPp2bWYgmgJQIXwl/Sp" crossorigin="anonymous">

        <!-- Latest compiled and minified JavaScript -->
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>

        <!--  jQuery -->
        <script type="text/javascript" src="https://code.jquery.com/jquery-1.11.3.min.js"></script>

        <!-- Bootstrap Date-Picker Plugin -->
        <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.4.1/js/bootstrap-datepicker.min.js"></script>
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.4.1/css/bootstrap-datepicker3.css"/>
       <!-- Home CSS 
        <link rel="stylesheet" type="text/css" href="homestyle.css">-->
        <!-- Include Bootstrap-select CSS, JS -->
        <link rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.6.2/css/bootstrap-select.min.css" />
        <script src="//cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.6.2/js/bootstrap-select.min.js"></script>
        <!-- Timepicker -->
        <link rel="stylesheet" href="pickadate.js-3.5.6/lib/themes/default.css">
        <link rel="stylesheet" href="pickadate.js-3.5.6/lib/themes/default.date.css">
        <link rel="stylesheet" href="pickadate.js-3.5.6/lib/themes/default.time.css">
        <script type="text/javascript" src="pickadate.js-3.5.6/lib/picker.js"></script>
        <script type="text/javascript" src="pickadate.js-3.5.6/lib/picker.date.js"></script>    
        <script type="text/javascript" src="pickadate.js-3.5.6/lib/picker.time.js"></script>
        
        <!-- Booking CSS -->
        <link rel="stylesheet" type="text/css" href="booking.css">
        <!--script.js-->
        <script type="text/javascript" src="script.js"></script>
    </head>
    <body>
        
    <div class="col-sm-3"></div>
    <div class="bootstrap-iso">
 <div class="container-fluid">
  <div class="row">
   <div class="col-sm-6">
   <legend><span class="number">1</span>Welcome <?php echo $_SESSION['username']; ?>, make a reservation</legend>
    <!-- Form code begins -->
    <form method="post" action="home.php">    
      
     <!-- Choose classrom -->
   <label for="sel1">Classroom</label>
   <select class="form-control" id="sel1" name="room" value="room" title="Choose a room">
       <option>Room 1</option>
       <option>Room 2</option>
       <option>Room 3</option>
       <option>Room 4</option>
</select>
     <!-- End Choose classrom -->
     <!-- Select you role -->
        <div class="form-style-5">
        <fieldset id="group1">
<label for="type">Select your role</label>
    
    <div class="radio">
  <label>
    <input type="radio" name="role" id="radioteaching" value="teaching" onclick="check()" required>
    Teaching
  </label>
</div>
<div class="radio">
  <label>
    <input type="radio" name="role" id="radioresearch" value="research" onclick="check()" >
    Research
  </label>
</div>
    <div class="radio">
  <label>
    <input type="radio" name="role" id="radiomaintenance" value="maintenance" onclick="check()">
    Maintenance
  </label>
</div>
<div class="radio">
  <label>
    <input type="radio" name="role" id="radiodemonstration" value="demonstration" onclick="check()">
    Demonstration
  </label>
</div>
    <div class="radio">
  <label>
    <input type="radio" name="role" id="radioother" value="other" onclick="check()">
    Other
  </label>
</div> 
</fieldset>
<fieldset>
    <div id="projectname">
    <input type="text" name="projectname" placeholder="Project Name">
    </div>
    </fieldset>
<fieldset>
    <div id="other">
    <input type="text" name="other" placeholder="Please, specify">
    </div>
    </fieldset>
<fieldset id="group2">
<label for="type">Will you be using any special equipment?</label>

 <div class="radio">
  <label>
    <input type="radio" name="specialequip" id="radioyes" value="Yes" onclick="check2()" required>
    Yes
  </label>
</div>
<div class="radio">
  <label>
    <input type="radio" name="specialequip" id="radiono" value="No" onclick="check2()">
    No
  </label>
</div>
<fieldset>
    <div id="expequip">
    <input type="text" name="expequip" placeholder="Specify which one">
    </div>
    </fieldset>    
</fieldset>
        </div>
         <!--Date picker FROM-->
        <div class="from">
        <label>From</label>
       <div class="date">
       <input class="datepicker" name="datefrom" type="text" id="prueba1" placeholder="Choose a date" required>
        <!-- Date configuration -->
        <script>
        $('.datepicker').pickadate({
          // Escape any “rule” characters with an exclamation mark (!).
          firstDay: 1,
          max: +30,
          min: true,
          format: 'dddd, dd mmm, yyyy',
          formatSubmit: 'yyyy,mm,dd',
          hiddenPrefix: 'prefix__',
          hiddenSuffix: '__suffix',
        
              
            onStart: function ()
          {
              var date = new Date();
             this.set('select', [date.getFullYear(), date.getMonth(), date.getDate()]);
          },
          hiddenName: true
        })
        </script>
       <script>$('.datepicker').pickadate();</script>
       </div>
        <!-- Time picker FROM -->
          <div class="time">
       <input class="timepicker" type="text" name="timefrom" placeholder="Choose a time">
              
               <?php
                session_start();
                //connect to database
                $db = mysqli_connect('mysql.hostinger.es', 'u717350551_user', '123456', 'u717350551_db');
                $sql = "SELECT datefrom, timefrom, dateto, timeto FROM reservations WHERE dateto >= CURDATE() ORDER BY datefrom ASC";
                $result = mysqli_query($db, $sql); 
                $counter = 1;
                while($test = mysqli_fetch_array($result))
                {
                    $varname = 'array'.$counter;
                    $$varname = array();
                    $$varname=[$test['datefrom'], $test['timefrom'], $test['dateto'], $test['timeto']];
                    echo $counter;
                    echo " espacio ";
                    echo $array1[0];
                    $counter = $counter+1;
                }
              ?>
              
       <script>$('.timepicker').pickatime({
          format: 'HH:i',       
          onOpen: function ()
          {    
              var a = "2017,4,23";
              var date = new Date(2016,3,20,9)
              var temp = new Date(document.getElementById("prueba1").value);
              var b =temp.getFullYear()+","+(temp.getMonth()+1)+","+temp.getDate();
               console.log(b);
              if(b.localeCompare(a) == 0){
                    this.set('disable', [
                    new Date(2016,3,20,4,30),
                    new Date(2016,3,20,9)
                  ] )}
              if(b.localeCompare(a) != 0){
                  this.set('enable', [
                    new Date(2016,3,20,4,30),
                    new Date(2016,3,20,9)
                  ] )}
              }, 
               
           });</script>
        </div>
            </div>
      <div class="checkbox">
      <label><input type="checkbox" id="checkmoredays" onclick="moredaysfunc()" value="">I want to reserve a room for more than a day</label>
    </div>
        <div class="to">
        <Label>To</Label>
        <div id="moredays" class="date">
       <input class="datepicker" type="text" name="dateto" id="prueba2" placeholder="Choose a date">
        <!-- Date picker TO -->
        <script>
        $('.datepicker').pickadate({
          // Escape any “rule” characters with an exclamation mark (!).
          firstDay: 1,
          max: +60,
          min: new Date(document.getElementById("prueba1").value),
          format: 'dddd, dd mmm, yyyy',
          formatSubmit: 'yyyy/mm/dd',
          hiddenPrefix: 'prefix__',
          hiddenSuffix: '__suffix',
          hiddenName: true,
           onOpen: function ()
          {
                
                this.set('min', new Date(document.getElementById("prueba1").value));
                //this.set('max', new Date(document.getElementById("prueba1").value) + 10);
          }
        })
        </script>
       <script>$('.datepicker').pickadate();</script>
        </div>
            
        <!-- Time picker TO -->
       <div class="time">
       <input class="timepicker" type="text" name="timeto" placeholder="Choose a time" required>
       <script>$('.timepicker').pickatime({
               format: 'HH:i',
           });</script>
       </div>
            </div>
        <!-- End Time picker -->
         
        <div class="form-group"> 
            <!-- Submit button -->
        <button class="btn btn-default" name="home_btn" type="submit" value="Home">Submit</button>
      </div>
        <!-- Form code ends -->
        </form>
      
    </div>
  </div>    
 </div>
</div>
        <div class="col-sm-3"></div>
    </body>
</html>		