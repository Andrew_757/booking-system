# README #

This code is a system to make reservations designed for the NTNU i Gjøvik Color Labs

This project is coded using HTML5, CSS and Bootstrap 3.3.7 for the front-end part of the page, in the back-end in order to communicate with MySQL database running version 5.5, it is used PHP 7.1 and the plugins that are used are Pickadate 3.5.6 and datatables 1.10.3 that make use of JQuery, which uses version 3.2.1. The application has been tested using the versions mentioned above.

In this folder the user can find the files mentioned before as well as a file named "sql_connect". This file is a template where the user needs to introduce its own database information (domain, name, password and database name), it must be included in every file that makes use of the database. 

For the database information, an sql file "database.sql" is in the folder. This contains the structure and creation of the tables as well as some examples of users and reservations that can be deleted for the final use of the application. This file can be used to create the same database structure in the transferred database by using the import option of phpMyAdmin.

Contact information:

- deguizabal94@gmail.com

- 96escalante@gmail.com

Developed by David Eguizábal Pérez and Andrés Escalante Ariza